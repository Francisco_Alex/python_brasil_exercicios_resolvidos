"""
Faça um Programa que peça o raio de um círculo, calcule e mostre sua área.
"""

raio = float(input('Digite o raio do círculo: '))

área = 3.14 * (raio ** 2)

print('A área de um círculo com %.1f de raio é de %i ' %(raio, área))
