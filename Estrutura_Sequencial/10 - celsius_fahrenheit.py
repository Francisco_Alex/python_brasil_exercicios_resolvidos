"""
faça um Programa que peça a temperatura em graus Celsius, transforme e mostre
em graus Farenheit.
"""

celsius = float(input('Digite a temperatura em graus Celsius: '))

fahrenheit = celsius * 9 / 5 + 32

print('%.2f graus Celsius para Fahrenheit é de %.2f °F ' %(celsius, fahrenheit))
