"""
Faça um Programa que calcule a área de um quadrado, em seguida mostre o dobro
desta área para o usuário.
"""

lado = int(input('Digite um lado do quadrado: '))

área = lado * lado

print('O dobro da área é de %i ' %(área * 2))
