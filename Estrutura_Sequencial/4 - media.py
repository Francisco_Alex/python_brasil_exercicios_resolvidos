"""
Faça um Programa que peça as 4 notas bimestrais e mostre a média.
"""

#Primeira forma
nota_1 = float(input('Digite a 1° nota: '))
nota_2 = float(input('Digite a 2° nota: '))
nota_3 = float(input('Digite a 3° nota: '))
nota_4 = float(input('Digite a 4° nota: '))

média = (nota_1 + nota_2 + nota_3 + nota_4) / 4

print('Média %.1f' %(média))

#--------------------------------------------

#Segunda forma
"""
quant = soma = 0

for i in range(1, 5):
    nota = float(input('Digite a %i° nota: ' %(i)))
    soma += nota
    quant += 1
    
print('Média %.1f' %(soma/quant))
"""
